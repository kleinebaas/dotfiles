#!/bin/bash

#
#  ~/.config/sway/waybar.sh
#  This file exists to (re)start waybar so it can use environment variables.

killall waybar
waybar -c ~/.config/waybar/themes/${THEME}/config -s ~/.config/waybar/themes/${THEME}/style.css & disown
