#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


### Shopt
#

shopt -s expand_aliases
shopt -s histappend


#
###

### Completion
#
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
complete -cf sudo
#
###

### Editor
#

if [[ -N $SSH_CONNECTION ]]; then
  export EDITOR='nano';
else
  export EDITOR='nano';
fi

export VISUAL='gedit';

# 
###

### ex - archive extractor
#   usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
#
###

### PS1
#
gitstatus() {
  if [ -d ".git" ] && [ -x "$(command -v git)"]; then
    echo ' [branch: $(git branch --show-current)]'
  fi
}


exitstatus()
{ 
    if [ $1 == "0" ]; then
        echo '[\e[32m:)\e[0m]'
    else
        echo "[\e[31mD:\e[0m] [$1]"
    fi
}

buildps1() {
  export PS1="$(exitstatus $?) [\w] [\u@\h]$(gitstatus)
[ \$> "
}

PROMPT_COMMAND=buildps1
#
###

### Aliases
#

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less
alias ls="ls --color=auto"

alias dotfiles="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

#
###

### Sources
#   nothing here yet!



#
###

source <(kitty + complete setup bash)
